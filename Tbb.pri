
message("Tbb-2017 in " $$PWD)

TBB_ARCH = x64
contains(QMAKE_HOST.arch, x86):TBB_ARCH = x32

win32:contains(QMAKE_HOST.os, Linux):TBB_ARCH = x32

CONFIG(release, debug|release): LIBS += -L$$PWD/$$TBB_ARCH -ltbb
else:CONFIG(debug, debug|release): LIBS += -L$$PWD/$$TBB_ARCH -ltbb_preview

INCLUDEPATH += $$PWD/inc/

DST_DIR=$$top_builddir

unix:!macx:COPY_FILES = $$PWD/$$TBB_ARCH/*.so
win32:COPY_FILES = $$PWD/$$TBB_ARCH/*.dll

contains(QMAKE_HOST.os,Windows): {
    COPY_FILES ~= s,/,\\,g
    DST_DIR ~= s,/,\\,g
}

QMAKE_POST_LINK += $$escape_expand(\n\t) $$QMAKE_COPY $$quote($$COPY_FILES) $$quote($$DST_DIR)
